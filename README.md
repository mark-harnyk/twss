See the answers below.

Contact me at: corporateanon@gmail.com

## Questions
1. A counter should increase by one on each click in http://jsbin.com/boyufo.
    - Why does it not work?
    - If you only could add code, what would you add to make the counter work?
    - You can get full overview of the code here: http://jsbin.com/boyufo/edit.

2. If you write your first name in http://jsbin.com/lezore this is reflected in the edit input.
    - If you edit your name in the edit input:
        1. The first input is not updated.
        2. The first input is no longer updating the second input.
    - What is happening and how do we fix it?
    - What actions or rules can we take to prevent this in the future?
    - You can get full overview of the code here: http://jsbin.com/lezore/edit.

3. A list is updated after two seconds in http://jsbin.com/decutu using `$timeout`.
    - The view is however not updated after two seconds.
    - What is happening and how do we fix it?
    - What actions or rules can we take to prevent this in the future?
    - You can get full overview of the code here: http://jsbin.com/decutu/edit.


4. Check out the folder `user-service` and open `user.service.js`
    - What improvements could be done on this service and generally on frontend?
    - What improvements could be done on backend?

5. Unit testing: Check out the folder `unit-testing`.
    - Install with `npm install` in a terminal.
    - You can run the test suite with `npm run test`.
    - You have to have Chrome installed in order to make that work.

There are 6 tests that fails.
Fix the issues in `product.model.js` without changing the spec. It's best to start with the first failing test and work the way down. When all tests are green, continue.

Add a function `setTaxRate` to set the tax rate.
    - Throw an error if the tax rate is not an integer or float
    - Throw an error if the input is lower than 0
    - Throw an error if the input is higher than 100
    - Make sure the tax rate is set by checking `product.taxRate`.

Add tests that cover all requirements.

##Answers
1. The counter is not increased, because digest is not performed in click event handler. Asynchronous callbacks, like DOM events, HTTP responses, WebSocket events, timeouts, etc., should call `$scope.$apply` explicitly if their results needs to be reflected in UI. Many Angular things follow this pattern so that for a developer thewy work transparently, for example, `ng-click` directive, `$timeout` service, `$http` service, etc. `$scope.$apply` starts digest algorythm that performs dirty-checking and reflects updated data on DOM.
    - the obvious solution is the following:

            #!javascript
            angular.module('conta', [])
              .directive('click', function () {
                return {
                    restrict: 'E',
                    scope: {
                        counter: '='
                    },
                    template: '<a href="#">Increase counter</a>',
                    link: function (scope, element) {
                        var a = element[0];

                        a.addEventListener('click', function () {
                          //wrap it with scope.$apply
                          scope.$apply(function () {
                            scope.counter++;
                          });
                        });
                    }
                };
            });

    **See**: http://jsbin.com/vicico/edit

    - it is not quite clear what is meant "if you could only add code", but let's imagine the worst case, when neither JS nor HTML cannot be touched. In this case a second directive with the same name can be created. Several Angular directives may share one name, and they both can work. Our second directive will add another click handler where digest is started with `$digest`:

            #!javascript
            //the code to be added
            angular.module('conta')
              .directive('click', function () {
                return {
                    priority: 1,
                    restrict: 'E',
                    link: function (scope, element) {
                        var a = element[0];
                        a.addEventListener('click', function () {
                            scope.$digest();
                        });
                    }
                };
            });

    **See**: http://jsbin.com/zukugi/edit

    - It is rather a workaround, and should not be used in real world projects.
    - Also, since the click event handlers are set with `addEventListener()`, a code that cleans up event listeners should be added. In `scope.$destroy()` we  have to call `removeEventListener()`.

2. An `ng-if` directive creates a child scope, thar inherits prototypically from its parent. In parent scope we define `name` property. It can be accessed in the child scope (where the second input is rendered). But when it is _assigned_ in the child scope, its own `name` property is created. It is no more associated with the one that is in the parent scope. That is why, when the second input sets `name`, it is not updated in the first one. Since the moment both inputs have their own `name` property, the first input cannot change `name` property of the second one.
    
    Besides that, in the example code the controller **is not used at all**, because there is no `ng-controller` directive in HTML. In this case I would write `ng-controller="Index as index"` and use the variables with namespace `index.*`. It makes code more clear. And it guarantees, that a case, when any property re-defined in a child scope, would never happen.

        <!DOCTYPE html>
        <html>
        <head
          <meta charset="utf-8">
          <title>JS Bin</title>
        </head>
        <body ng-app="conta" ng-controller="Index as index">
          <b>Write your name:</b>
          <br>
          <input type="text" ng-model="index.name">
          <br>
          <div ng-if="index.name">
              <b>Edit your name:</b><br>
              <input type="text" ng-model="index.name">
          </div>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js"></script>
        </body>
        </html>

    The controller will look like this:

        #!javascript
        angular.module('conta', [])
            .controller('Index', function () {
                this.name = '';
            });

    **See**: http://jsbin.com/popije/edit

3. In the controller the statement `$scope.list = users.list;` assigns the reference of `list` array of `users` service to `list` property of controller's scope. It is ok, until the service re-assigns its `list` property in `fetch` method. After that happened, the `$scope.list` is a reference to the first array, while `users.list` - to the second (an updated one). Actually, there are lots of ways how to avoid this behavior. It depends of the requirements to `users` service. Let's consider several of them:
    1. Leave a reference to an array untouched and only update its elements:
    
        Change:

            service.list = data;

        To:

            angular.copy(data, service.list);

        **See**: http://jsbin.com/vurihi/edit

    2. Go one level deeper. Make `users.state` property, which is accessed from controller and which reference is never re-assigned. Store the result under this `users.state`.

        **See**: http://jsbin.com/sokife/edit

    3. Personally I prefer to use pure functions with no side effects. In this case the service would just return new list instead of changing anything outside the `fetch()` method.
        
        **See**: http://jsbin.com/fayiti/edit

4. user.service.js
    - First, there is no need in using `deferred`, since we have `$q.reject()`. So, our response handlers will look like the following:

            .then(function (response) {
                var data = response.data;

                if (response.data.success) {
                    return data;
                } else {
                    return $q.reject(data);
                }
            });

        There are some mistakes in the dependencies. `$rootScope` can be removed. `$` is probably a typo, must be replaced with `$q`.

        **See the improved version**: `user-service/user.service-1.js`.

    - We could also make an $http-interceptor, that would set the headers instead of doing that manually in each service method. In this case, there should be a `session` or `auth` service, that would store a token. The interceptor would take a token from this service and place it to HTTP-request. The example code is omitted.

    - We could also change `Content-Type` to `application/json`. As for me, it looks more modern and simple than `application/x-www-form-urlencoded`. In this case we shouldn't pass `Content-Type` header at all.

    - A backend could be made more RESTful. First of all, it can return HTTP status codes instead of setting `success` property to `false` or `true`.

        For exapmple:

        - if the user we are trying to create has invalid `username` property, the backend will return `400 Bad Request`
        - if we try to change password of the user that does not exist, the server will return `404 Not Found`
        - etc
        
        In REST we deal with resources rather than actions, so I would suggest the following backend routing scheme:

        - `POST /api/v2/users` create a user.
        - `PUT /api/v2/users/:id` update a user (all properties).
        - `PATCH /api/v2/users/:id` update a user (several properties). As we see, it can be used for changing a password.
        - `PUT /api/v2/users/:id/password` - alternative way to change a password. In this approach we consider a password to be user's nested resource.
        - `DELETE /api/v2/users/:id` - delete a user.
        - `GET /api/v2/users/:id` - get a user.
        - `GET /api/v2/users` - get all users.
        
        **See the RESTful version**: `user-service/user.service-1.js`.

5. Unit testing. The bugs are fixed and `setTaxRate()` method is implemented and covered with unit tests.

    **See**: `unit-testing/`

