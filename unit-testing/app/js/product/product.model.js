(function () {
    'use strict';

    angular.module('conta').factory('Product', ProductModel);

    /**
     * @ngdoc factory
     * @name ProductModel
     */
    function ProductModel() {
        /**
         * Defaults
         */
        var defaults = {
            taxRate: 15
        };

        /**
         * Product Constructor.
         *
         * @param {Object} properties
         * @constructor
         */
        function Product(properties) {
            angular.extend(this, defaults, properties);
        }

        /**
         * Public
         */
        angular.extend(Product.prototype, {
            getPriceIncVat: getPriceIncVat,
            setTaxRate: setTaxRate
        });

        /**
         * Return constructor
         */
        return Product;

        /**
         * Get price incl. vat.
         *
         * @returns {Number}
         */
        function getPriceIncVat() {
            return Math.floor(this.price * (100 + this.taxRate)) / 100;
        }

        /**
         * Set tax rate
         *
         * @param {Number} taxRate
         */
        function setTaxRate(taxRate) {
            if(typeof taxRate !== 'number' || isNaN(taxRate)) {
                throw new TypeError('taxRate is not a number');
            }
            if(taxRate > 100 || taxRate < 0) {
                throw new RangeError('taxRate is not in range [0...100]');
            }
            this.taxRate = taxRate;
        }
    }
}());
