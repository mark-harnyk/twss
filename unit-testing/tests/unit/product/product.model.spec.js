describe('Model: Product', function () {
    var product, model, Product;

    beforeEach(function () {
        module('conta');

        inject(function ($injector) {
            Product = $injector.get('Product');
        });

        model = {
            name: 'Beer',
            taxRate: 8.975,
            price: 19.99
        };

        product = new Product(model);
    });

    it('should be an instance of Product', function () {
        expect(product instanceof Product).toBeTruthy();
    });

    describe('on initialization', function () {
        it('should set default values on an empty product', function () {
            product = new Product();

            expect(product.taxRate).toBe(15);
        });

        it('should set model on the product', function () {
            expect(product.name).toBe('Beer');
            expect(product.price).toBe(19.99);
            expect(product.taxRate).toBe(8.975);
        });

        it('should override any default values', function () {
            model = { taxRate: 55 };

            product = new Product(model);

            expect(product.taxRate).toBe(55);
        });

        it('should set default values on the product if they are missing', function () {
            model = { name: 'Beer' };

            product = new Product(model);

            expect(product.name).toBe(model.name);
            expect(product.taxRate).toBe(15);
        });

        it('should set and get any unknown attribute', function() {
            var key = 'random' + (new Date()).getTime();

            model[key] = true;

            product = new Product(model);

            expect(product[key]).toBeTruthy();
        });
    });

    describe('getPriceIncVat()', function () {
        it('should return the price incl. VAT with two decimals', function () {
            expect(product.getPriceIncVat()).toBe(21.78);
        });
    });

    describe('setTaxRate(taxRate)', function () {
        it('should throw an error when taxRate is not a number', function () {
            expect(function () {
                product.setTaxRate('string');
            }).toThrowError();

            expect(function () {
                product.setTaxRate('14.5');
            }).toThrowError();

            expect(function () {
                product.setTaxRate(undefined);
            }).toThrowError();

            expect(function () {
                product.setTaxRate(NaN);
            }).toThrowError();

            expect(function () {
                product.setTaxRate({});
            }).toThrowError();
        });

        it('should throw an error when taxRate is less than 0', function () {
            expect(function () {
                product.setTaxRate(-0.1);
            }).toThrowError();
        });

        it('should throw an error when taxRate is greater than 100', function () {
            expect(function () {
                product.setTaxRate(100.1);
            }).toThrowError();
        });

        it('should set taxRate', function () {
            product.setTaxRate(40.01);
            expect(product.taxRate).toBe(40.01);
        });
    });
});
