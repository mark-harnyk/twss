angular.module('conta').factory('user', ['$q', '$http', function ($q, $http) {
    return {
        /**
         * Create a new user
         * @param {Object} data
         * @param {String} token
         * @returns {promise}
         */
        createUser: function (data, token) {
            return $http({
                method: 'POST',
                url: '/api/v1/user/create',
                data: data,
                headers: {
                    'Auth-Token': token,
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                var data = response.data;

                if (data.success) {
                    return data;
                } else {
                    return $q.reject(data);
                }
            });
        },

        /**
         * Change password
         * @param {Object} data
         * @param {String} token
         * @returns {promise}
         */
        changePassword: function (data, token) {
            return $http({
                method: 'POST',
                url: '/api/v1/user/change-password',
                data: data,
                headers: {
                    'Auth-Token': token,
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                var data = response.data;

                if (data.success) {
                    return data;
                } else {
                    return $q.reject(data);
                }
            });
        }
    }
}]);
