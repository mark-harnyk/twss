angular.module('conta').factory('user', ['$q', '$http', 'app.errors', function ($q, $http, errors) {
    
    /**
     * HTTP error handler.
     * The code is intentionally simplified in order to illustrate.
     */
    function handleErrors(response) {
        //A server is responded with well-formed list of errors.
        //We can return it.
        if(response.data && response.data.errors) {
            return $q.reject(new errors.ServerError(response.data.errors));
        }
        //No correct error response is received from the server
        //It may be a network problem or the server is down, or something else.
        return $q.reject(new errors.NetworkError(response));
    }

    //NOTE: It is assumed, that Auth-Token is set outside of this service, in an interceptor.

    return {
        /**
         * Create a new user
         * @param {Object} data
         * @returns {promise}
         */
        createUser: function (data) {
            return $http({
                method: 'POST',
                url: '/api/v2/users',
                data: data
            }).then(function (response) {
                return response.data;
            }, handleErrors);
        },

        /**
         * Change password
         * @param {Number} id
         * @param {String} password
         * @returns {promise}
         */
        changePassword: function (id, password) {
            return $http({
                method: 'PUT',
                url: '/api/v2/users/' + id + '/password',
                data: password
            }).then(function (response) {
                return response.data;
            }, handleErrors);
        }
    }
}]);
