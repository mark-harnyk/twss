angular.module('conta').factory('user', ['$rootScope', '$', '$http', function ($rootScope, $q, $http) {
    return {
        /**
         * Create a new user
         * @param {Object} data
         * @param {String} token
         * @returns {promise}
         */
        createUser: function (data, token) {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: '/api/v1/user/create',
                data: data,
                headers: {
                    'Auth-Token': token,
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                var data = response.data;

                if (response.data.success) {
                    deferred.resolve(data);
                } else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        },

        /**
         * Change password
         * @param {Object} data
         * @param {String} token
         * @returns {promise}
         */
        changePassword: function (data, token) {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: '/api/v1/user/change-password',
                data: data,
                headers: {
                    'Auth-Token': token,
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                var data = response.data;

                if (response.data.success) {
                    deferred.resolve(data);
                } else {
                    deferred.reject(data);
                }
            });

            return deferred.promise;
        }
    }
}]);
